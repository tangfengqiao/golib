package golib

import "github.com/go-ini/ini"

type (
	Map      map[string]interface{}
	ConfFile struct {
		Cfg   *ini.File
		IsDev bool
		Error error
	}
)

//Conf 读取配置信息
func Conf(section string) map[string]string {
	conf := &ConfFile{IsDev: false}
	if ok, _ := FileExists("./dev.ini"); ok {
		conf.IsDev = true
		conf.Cfg, conf.Error = ini.Load("./dev.ini")
	} else {
		conf.IsDev = false
		conf.Cfg, conf.Error = ini.Load("./config.ini")
	}

	if conf.Error != nil {
		Log("配置文件加载错误!", conf.Error)
	}
	return conf.Cfg.Section(section).KeysHash()
}
