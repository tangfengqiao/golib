package golib

import (
	"crypto/md5"
	"encoding/hex"
	"net/url"
	"sort"
)

// 对url参数签名,sign参数排除签名
func Sign(query url.Values, secretKey string) string {
	// 按照key的字典顺序进行排序
	var keys []string
	for k := range query {
		if k != "sign" {
			keys = append(keys, k)
		}
	}
	sort.Strings(keys)

	signStr := ""
	for _, k := range keys {
		signStr += k + query.Get(k)
	}
	signStr += secretKey

	// 使用md5进行加密
	hash := md5.Sum([]byte(signStr))
	return hex.EncodeToString(hash[:])
}
