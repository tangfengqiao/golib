package golib

import (
	"github.com/sirupsen/logrus"
)

func Log(log ...interface{}) {
	logrus.Infoln(log...)
}
