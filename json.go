package golib

import "encoding/json"

//JsonToMap json转map
func JsonToMap(str string) Map {
	data := Map{}
	json.Unmarshal([]byte(str), &data)
	return data
}

//JsonToStruct json转结构体
func JsonToStruct(obj interface{}, str string) interface{} {
	json.Unmarshal([]byte(str), &obj)
	return obj
}

//ObjToJson map和struct转json
func ObjToJson(obj interface{}) string {
	mjson, _ := json.Marshal(obj)
	return string(mjson)
}
