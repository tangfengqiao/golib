package golib

import (
	"fmt"

	"github.com/aliyun/alibaba-cloud-sdk-go/services/dysmsapi"
)

//SendSms 发短信
func SendSms(phone string, TemplateCode string, param string) bool {
	conf := Conf("sms")
	if conf["type"] != "aliyun" {
		return true
	}
	client, err := dysmsapi.NewClientWithAccessKey(conf["regionid"], conf["accessid"], conf["secret"])

	request := dysmsapi.CreateSendSmsRequest()
	request.Scheme = "https"

	request.PhoneNumbers = phone
	request.SignName = conf["sign"]
	request.TemplateCode = TemplateCode
	request.TemplateParam = param

	response, err := client.SendSms(request)
	if err != nil {
		Log(err)
		return false
	} else {
		Log(fmt.Sprintf("send sms response is %#v\n", response))
		return true
	}
}
