package golib

import (
	"strings"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

//Mysql 连接数据库
func Mysql() *gorm.DB {
	conf := Conf("db")
	db, err := gorm.Open(mysql.Open(conf["mysql_dsn"]), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		}})
	if err != nil {
		Log("数据库错误", err)
	} else {
		tmp := strings.Split(conf["mysql_dsn"], "@")
		Log("mysql连接成功", "******@"+tmp[1])
	}
	DB, err2 := db.DB()
	if err2 != nil {
		Log("mysql错误:", err2)
	}
	DB.SetMaxIdleConns(10)           //闲置的连接数
	DB.SetMaxOpenConns(100)          //最大打开的连接数
	DB.SetConnMaxLifetime(time.Hour) //连接可复用的最大时间
	return db
}
